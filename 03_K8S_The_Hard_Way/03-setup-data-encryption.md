## What is Data Encryption Config in K8S? 

Kubernetes offers the ability to encrypt secret data at rest instead of storing it as plain text.\
But for this of course an encryption key is needed. 

In order to setup all that, we need to generate data encryption config for K8S that includes 
such encryption key and distribute it to the controller instances. 

## Generating Data Encryption Config 

1.  Generate encryption key from random string and store it in variable:\
    `ENCRYPTION_KEY=$(head -c 32 /dev/urandom | base64)`

1.  Generate the config using the encryption key variable:\
    ```shell
    cat > encryption-config.yaml << EOF 
    kind: EncryptionConfig 
    apiVersion: v1 
    resources: 
      - resources: 
          - secrets 
        providers: 
          - aescbc: 
              keys: 
                - name: key1 
                  secret: ${ENCRYPTION_KEY} 
          - identity: {} 
    EOF
    ```

2.  Copy the generated config to the controller nodes:\
    `scp encryption-config.yaml <USER>@<MASTER>:~`