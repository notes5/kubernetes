## Why do need CA and TLS certs? 

-   **Certificates** - used to validate identity 
-   **Certificate Authority** - can confirm if a certificate is valid. 

K8S uses certificates for variety of security functions so it needs CA to 
issue them and then use to validate their validity. 

## What certificates need to be issued from the CA? 

-   **Client Certificates** - To provide authentication to the K8S API for various users 
(admin, kube-controller-manager, kube-proxy, kube-scheduler and the kubelet 
client on each node) = components could prove their identity to the API 
-   **K8S API Server Certificate** - The TLS cert. for the K8S API = API server could 
prove its identity to the components 
-   **Service Account Key Pair** - Used by K8S to sign service account tokens  

#### Creating CA: 

1.  Create a file called ca-config.json with that content:

    ```json
    { 
      "signing": { 
        "default": { 
          "expiry": "8760h" 
        }, 
        "profiles": { 
          "kubernetes": { 
            "usages": ["signing", "key encipherment", "server auth", "client auth"], 
            "expiry": "8760h" 
          } 
        } 
      } 
    }
    ```

2.  Create a file called ca-csr.json with that content:

    ```json
    { 
      "CN": "Kubernetes", 
      "key": { 
        "algo": "rsa", 
        "size": 2048 
      }, 
      "names": [ 
        { 
          "C": "US", 
          "L": "Portland", 
          "O": "Kubernetes", 
          "OU": "CA", 
          "ST": "Oregon" 
        } 
      ] 
    }
    ```

3.  Create the CA:\
    `cfssl gencert -initca ca-csr.json | cfssljson -bare ca`

4.  From the files that are generated you will need: 

    1.  **ca-key.pem** - the private certificate for the CA 

    2.  **ca.pem** - the  public certificate for the CA

_**Note:**_ You can use the below commands to easily generate the json files and edit them accordingly to be like the above examples:

```shell
cfssl print-defaults config > ca-config.json 
cfssl print-defaults csr > ca-csr.json
```

#### Generate Client Certs: 

1.  Generate default CSR json for each user (admin, kubelet, kube-controller-manager,
 kube-proxy, kube-scheduler).\
    For kubelet a certificate is needed for each node:\
    ```shell
    cfssl print-defaults csr > <USER>-csr.json
    cfssl print-defaults csr > <NODE>-csr.json
    ```

2.  Adjust `admin-csr.json` and generate cert: 

    1.  Edit the json so that: 

        1.  key is `rsa/2048` 
        2.  `CN: "admin"` 
        3.  hosts block is removed 
        4.  `"O": "system:masters"` 

    2.  Generate the certificate:
        ```shell
        cfssl gencert -ca=ca.pem \
        -ca-key=ca-key.pem \
        -config=ca-config.json \
        -profile=kubernetes admin-csr.json | cfssljson -bare admin
        ```

3.  Adjust `<NODE>-csr.json` and generate cert: 

    1.  Edit the json so that: 

        1.  key is `rsa/2048` 
        2.  `CN: "system:node:<NODE-HOSTNAME>"` 
        3.  hosts block is removed 
        4.  `"O": "system:nodes"` 

    2.  Generate certificate for each node:
        ```shell
        cfssl gencert -ca=ca.pem \
        -ca-key=ca-key.pem \
        -config=ca-config.json \
        -hostname=<NODE-IP>,<NODE-HOSTNAME> \
        -profile=kubernetes <NODE>-csr.json | cfssljson -bare <NODE-HOSTNAME>
        ```

4.  Adjust `kube-controller-manager-csr.json` and generate cert: 

    1.  Edit the json so that: 

        1.  key is `rsa/2048` 
        2.  `"CN": "system:kube-controller-manager"` 
        3.  Hosts block is removed 
        4.  `"O": "system:kube-controller-manager"` 

    2.  Generate the certificate:
        ```shell
        cfssl gencert -ca=ca.pem \
        -ca-key=ca-key.pem \
        -config=ca-config.json \
        -profile=kubernetes kube-controller-manager-csr.json | cfssljson -bare kube-controller-manager
        ``` 

5.  Adjust `kube-proxy-csr.json` and generate cert: 

    1.  Edit the json so that: 

        1.  key is `rsa/2048` 
        2.  `"CN": "system:kube-proxy"` 
        3.  Hosts block is removed 
        4.  `"O": "system:node-proxier"` 

    2.  Generate the certificate:
        ```shell
        cfssl gencert -ca=ca.pem \
        -ca-key=ca-key.pem \
        -config=ca-config.json \
        -profile=kubernetes kube-proxy-csr.json | cfssljson -bare kube-proxy
        ```

6.  Adjust `kube-scheduler-csr.json` and generate cert: 

    1.  Edit the json so that: 

        1.  key is `rsa/2048` 
        2.  `"CN": "system:kube-scheduler"` 
        3.  Hosts block is removed 
        4.  `"O": "system:kube-scheduler"` 

    2.  Generate the certificate:
        ```shell
        cfssl gencert -ca=ca.pem \
        -ca-key=ca-key.pem \
        -config=ca-config.json \
        -profile=kubernetes kube-scheduler-csr.json | cfssljson -bare kube-scheduler
        ```

#### Generating the Kubernetes API Server Certificate: 

1.  Generate default CSR json with cfssl:\
    `cfssl print-defaults csr > kubernetes-csr.json`

2.  Edit the json so that: 

    1.  key is `rsa/2048` 
    2.  `"CN": "kubernetes"` 
    3.  Hosts block is removed 
    4.  `"O": "kubernetes"` 

3.  Create comma-separated-list bash variable holding the hostnames
 and IPs of the controller instances and the nginx load labancer 
 instance + 10.32.0.1, localhost & kubernetes.default:\
    `CERT_HOSTNAME=10.32.0.1,<MASTER01-IP>,<MASTER01-HOSTNAME>,<MASTER02-IP>,<MASTER02-HOSTNAME>,<LB-IP>,<LB-HOSTNAME>,127.0.0.1,localhost,kubernetes.default`

4.  Generate the certificate, passing the variable above to the hostname option:
    ```shell
    cfssl gencert -ca=ca.pem \
    -ca-key=ca-key.pem \
    -config=ca-config.json \
    -hostname=$CERT_HOSTNAME \
    -profile=kubernetes kubernetes-csr.json | cfssljson -bare kubernetes
    ```

#### Generating the Service Account Key Pair: 

1.  Generate default CSR json with cfssl:\
    `cfssl print-defaults csr > service-account-csr.json`

2.  Edit the json so that: 

    1.  key is `rsa/2048` 
    2.  `"CN": "service-accouts"` 
    3.  Hosts block is removed 
    4.  `"O": "kubernetes"` 

3.  Generate the certificate:
    ```shell
    cfssl gencert -ca=ca.pem \
    -ca-key=ca-key.pem \
    -config=ca-config.json \
    -profile=kubernetes service-account-csr.json | cfssljson -bare service-account
    ```

#### Distributing the certificates: 

1.  Copy the CA's public key and the specific node client certificate's 
public and private keys to the node (for each node):\
    `scp ca.pem <NODE-HOSTNAME>-key.pem <NODE-HOSTNAME>.pem <USER>@<NODE>:~`

2.  Copy the CA, API server and service accounts' public and private keys to each controller:
    ```shell
    scp ca.pem ca-key.pem \
    kubernetes-key.pem kubernetes.pem \
    service-account-key.pem service-account.pem <USER>@<MASTER>:~
    ```