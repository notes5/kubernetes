## What does DNS do inside pod network?

* Provides DNS service to be used by pods within the network
* Configures containers to use the DNS service to do lookups

_**TL;DR!**_ You can access services and pods via their DNS names.

## Deploying

On the kubectl configured instance do:

1. Deploy CoreDNS: \
`kubectl apply -f https://storage.googleapis.com/kubernetes-the-hard-way/coredns.yaml`

2. Verify: \
`kubectl get pods -l k8s-app=kube-dns -n kube-system`