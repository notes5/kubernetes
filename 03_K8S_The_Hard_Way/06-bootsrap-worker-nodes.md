## What are the worker nodes?

They run the actual workload on the cluster.
They are managed by the control plane.

## Worker node components

* **kubelet** - Controls each worker node, providing the APIs that are used by the
control plane to manage nodes and pods. Also interacts with the container runtime
to manage the containers on the nodes.

* **kube-proxy** - Manages the iptables rules on the node to provide virtual
network access to pods.

* **Container Runtime** - Downloads images and runs containers (i.e. Docker)

## Installing the Worker Node binaries

#### Setting up the container runtime (containerd)

1. Carry out the prerequisites:\
    ```shell
    ### Install prerequisites
    sudo apt install socat conntrack ipset
    
    ### Load the necessary modules
    sudo bash -c 'cat > /etc/modules-load.d/containerd.conf <<EOF
    overlay
    br_netfilter
    EOF'
    
    sudo modprobe overlay
    sudo modprobe br_netfilter
    
    ### Setup required sysctl params
    sudo bash -c 'cat > /etc/sysctl.d/99-kubernetes-cri.conf <<EOF
    net.bridge.bridge-nf-call-iptables  = 1
    net.ipv4.ip_forward                 = 1
    net.bridge.bridge-nf-call-ip6tables = 1
    EOF'
    
    sudo sysctl --system
   
    ### Create the necessary directories
    sudo mkdir -p \
     /etc/cni/net.d \
     /opt/cni/bin \
     /etc/containerd
    ```

2. Install `cni-plugins` (change version accordingly by checking
<https://github.com/containernetworking/plugins/releases>):
    ```shell
    VERSION="v0.8.5"
    wget https://github.com/containernetworking/plugins/releases/download/$VERSION/cni-plugins-linux-amd64-$VERSION.tgz
    sudo tar -xvf cni-plugins-linux-amd64-$VERSION.tgz -C /opt/cni/bin/
    ```

3. Install `crictl` (change version accordingly by checking
<https://github.com/kubernetes-sigs/cri-tools/releases>):
    ```shell
    VERSION="v1.17.0"
    wget https://github.com/kubernetes-sigs/cri-tools/releases/download/$VERSION/crictl-$VERSION-linux-amd64.tar.gz
    sudo tar zxvf crictl-$VERSION-linux-amd64.tar.gz -C /usr/local/bin
    ```

4. Install `runc` (change version accordingly by checking
<https://github.com/opencontainers/runc/releases>):
    ```shell
    VERSION="v1.0.0-rc10"
    wget https://github.com/opencontainers/runc/releases/download/$VERSION/runc.amd64
    sudo chmod +x runc.amd64
    sudo mv runc.amd64 /usr/local/bin/runc
    ```

5. Install `containerd` (change version accordingly by checking
<https://containerd.io/downloads/>):
    ```shell
    VERSION="1.3.3"
    wget https://github.com/containerd/containerd/releases/download/v$VERSION/containerd-$VERSION.linux-amd64.tar.gz
    sudo tar -xvf containerd-$VERSION.linux-amd64.tar.gz -C /
    ```
   
#### Setting up the kubernetes binaries

1. Create the needed directories:
    ```shell
    sudo mkdir -p \
      /var/lib/kubelet \
      /var/lib/kube-proxy \
      /var/lib/kubernetes \
      /var/run/kubernetes
    ```
   
2. Setup the binaries (change version accordingly by checking
<https://github.com/kubernetes/kubernetes/releases>):
    ```shell
    ### Download
    VERSION="v1.17.0"
    wget https://storage.googleapis.com/kubernetes-release/release/$VERSION/bin/linux/amd64/kubectl
    wget https://storage.googleapis.com/kubernetes-release/release/$VERSION/bin/linux/amd64/kube-proxy
    wget https://storage.googleapis.com/kubernetes-release/release/$VERSION/bin/linux/amd64/kubelet
    
    ### Setup
    sudo chmod +x kubectl kube-proxy kubelet
    sudo mv kubectl kube-proxy kubelet /usr/local/bin/
    ```   

## Configuring containerd, kubelet & kube-proxy

#### Setup containerd

1. Setup the configuration
    ```shell
    cat << EOF | sudo tee /etc/containerd/config.toml
    [plugins]
      [plugins.cri.containerd]
        snapshotter = "overlayfs"
        [plugins.cri.containerd.default_runtime]
          runtime_type = "io.containerd.runtime.v1.linux"
          runtime_engine = "/usr/local/bin/runc"
          runtime_root = ""
    EOF
    ```

2. Setup the systemd unit file
    ```shell
    cat <<EOF | sudo tee /etc/systemd/system/containerd.service
    [Unit]
    Description=containerd container runtime
    Documentation=https://containerd.io
    After=network.target
    
    [Service]
    ExecStartPre=/sbin/modprobe overlay
    ExecStart=/bin/containerd
    Restart=always
    RestartSec=5
    Delegate=yes
    KillMode=process
    OOMScoreAdjust=-999
    LimitNOFILE=1048576
    LimitNPROC=infinity
    LimitCORE=infinity
    
    [Install]
    WantedBy=multi-user.target
    EOF
    ```
   
#### Setup kubelet

1. Move the keys to the necessary directories:
    ```shell
    HOSTNAME=$(hostname)
    sudo mv ${HOSTNAME}-key.pem ${HOSTNAME}.pem /var/lib/kubelet/
    sudo mv ${HOSTNAME}.kubeconfig /var/lib/kubelet/kubeconfig
    sudo mv ca.pem /var/lib/kubernetes/
    ```

2. Create kubelet's config file
    ```shell
    cat << EOF | sudo tee /var/lib/kubelet/kubelet-config.yaml
    kind: KubeletConfiguration
    apiVersion: kubelet.config.k8s.io/v1beta1
    authentication:
      anonymous:
        enabled: false
      webhook:
        enabled: true
      x509:
        clientCAFile: "/var/lib/kubernetes/ca.pem"
    authorization:
      mode: Webhook
    clusterDomain: "cluster.local"
    clusterDNS: 
      - "10.32.0.10"
    runtimeRequestTimeout: "15m"
    tlsCertFile: "/var/lib/kubelet/${HOSTNAME}.pem"
    tlsPrivateKeyFile: "/var/lib/kubelet/${HOSTNAME}-key.pem"
    EOF
    ```

3. Create kubelet's systemd unit file:
    ```shell
    cat << EOF | sudo tee /etc/systemd/system/kubelet.service
    [Unit]
    Description=Kubernetes Kubelet
    Documentation=https://github.com/kubernetes/kubernetes
    After=containerd.service
    Requires=containerd.service
    
    [Service]
    ExecStart=/usr/local/bin/kubelet \\
      --config=/var/lib/kubelet/kubelet-config.yaml \\
      --container-runtime=remote \\
      --container-runtime-endpoint=unix:///var/run/containerd/containerd.sock \\
      --image-pull-progress-deadline=2m \\
      --kubeconfig=/var/lib/kubelet/kubeconfig \\
      --network-plugin=cni \\
      --register-node=true \\
      --v=2 \\
      --hostname-override=${HOSTNAME} \
    Restart=on-failure
    RestartSec=5
    
    [Install]
    WantedBy=multi-user.target
    EOF
    ```

#### Setup kube-proxy

1. Move the kubeconfig to the necessary place:\
`sudo mv kube-proxy.kubeconfig /var/lib/kube-proxy/kubeconfig`

2. Create kube-proxy config file:
    ```shell
    cat << EOF | sudo tee /var/lib/kube-proxy/kube-proxy-config.yaml
    kind: KubeProxyConfiguration
    apiVersion: kubeproxy.config.k8s.io/v1alpha1
    clientConnection:
      kubeconfig: "/var/lib/kube-proxy/kubeconfig"
    mode: "iptables"
    clusterCIDR: "10.200.0.0/16"
    EOF
    ```

3. Create the systemd unit file:
    ```shell
    cat << EOF | sudo tee /etc/systemd/system/kube-proxy.service
    [Unit]
    Description=Kubernetes Kube Proxy
    Documentation=https://github.com/kubernetes/kubernetes
    
    [Service]
    ExecStart=/usr/local/bin/kube-proxy \\
      --config=/var/lib/kube-proxy/kube-proxy-config.yaml
    Restart=on-failure
    RestartSec=5
    
    [Install]
    WantedBy=multi-user.target
    EOF
    ```
   
## Start the configured services

1. Reload systemd, start and enable the configured services:
```shell
sudo systemctl daemon-reload
sudo systemctl enable containerd kubelet kube-proxy --now
```   

2. Check the nodes list from the control plane:\
`kubectl get nodes`

    _**NOTE:**_ The nodes are expected to be in `NotReady` state!