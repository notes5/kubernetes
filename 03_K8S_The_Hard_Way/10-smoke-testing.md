## Check if the cluster works

#### Check if data is encrypted

1. Create dummy test secret: \
`kubectl create secret generic kthw --from-literal="mykey=mydata"`

2. Test if it's encrypted:
    * Login to one of the controllers
    * Execute:
        ```shell
        sudo ETCDCTL_API=3 etcdctl get \
         --endpoints=https://127.0.0.1:2379 \
         --cacert=/etc/etcd/ca.pem \
         --cert=/etc/etcd/kubernetes.pem \
         --key=/etc/etcd/kubernetes-key.pem\
         /registry/secrets/default/kthw | hexdump -C
        ```
3. If you see `k8s:enc:aescbc:v1:key1` on the gibberish
in the right column then we are good.

#### Test deployments

1. Create nginx deployment: \
`kubectl run nginx --image=nginx`

2. Verify that the deployment created a pod: \
`kubectl get pods -l run=nginx`

#### Test port forwarding

1. Make sure socat is installed on each worker node: \
`sudo apt install socat`

2. Store the nginx pod's name in a variable: \
`POD_NAME=$(kubectl get pods -l run=nginx -o jsonpath="{.items[0].metadata.name}")`

3. Forward the nginx port on the pod to 8081: \
`kubectl port-forward $POD_NAME 8081:80`

3. Test if the forwarding works
(should return OK / HTTP Code 200): \
`curl --head http://127.0.0.1:8081`

#### Test logs

1. Test if the nginx pod logged the curl request above
(should return log entry): \
`kubectl logs $POD_NAME`

#### Test exec

1. Test if `kubectl exec` works with the nginx pod
(should return the nginx version): \
`kubectl exec -ti $POD_NAME -- nginx -v`

#### Test services

1. Create NodePort service for the nginx deployment: \
`kubectl expose deployment nginx --port 80 --type NodePort`

2. Confirm the service is created and copy the node port: \
`kubectl get svc`

3. Login to a worker node and curl using the node port
(should return OK / HTTP Code 200): \
`curl -I localhost:<NODE-PORT>`

## Clean up

```shell
kubectl delete secret kthw
kubectl delete svc nginx
kubectl delete deployment nginx
```