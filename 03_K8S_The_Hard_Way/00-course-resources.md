## What we are going to build:

![cluster-diagram](images/kthw-diagram.png)

## Necessary Tools:

<https://github.com/kelseyhightower/kubernetes-the-hard-way/blob/master/docs/02-client-tools.md>

##### CFSSL

_Replace VERSION with what you need (i.e. R1.2)_ 

```shell
wget https://pkg.cfssl.org/<VERSION>/cfssl_linux-amd64
wget https://pkg.cfssl.org/<VERSION>/cfssljson_linux-amd64
sudo chmod +x cfssl_linux-amd64 cfssljson_linux-amd64 
sudo mv cfssl_linux-amd64 /usr/local/bin/cfssl 
sudo mv cfssljson_linux-amd64 /usr/local/bin/cfssljson 
cfssl version 
```
 
##### kubectl

_Replace VERSION with what you need (i.e. v1.18.0)_

```shell
wget https://storage.googleapis.com/kubernetes-release/release/<VERSION>/bin/linux/amd64/kubectl 
chmod +x kubectl 
sudo mv kubectl /usr/local/bin/ 
kubectl version --client 
```
 
## LAB setup:

* Setup 6 machines in AWS from Ubuntu 16.04 with the same AWS key-pair: 
    * bastion.k8s.local - to access the cluster nodes and operate with kubectl
    * nginx.k8s.local - to be setup as the API endpoint
    * master01.k8s.local - control plane 
    * master02.k8s.local - control plane
    * node01.k8s.local - worker node
    * node02.k8s.local - worker node
* Run everything except bastion in private subnet (with NAT gateway)
* Run the bastion in public subnet and associate Elastic IP for persistence
* Setup private hosted zone in Route53 for k8s.local 
and add CNAME records for each instance as follows:\
`<INSTANCE>.k8s.local -> the AWS DNS name of the instance (visible in EC2)`
* Add the private key from the AWS key pair used to the bastion
 so that you can easily ssh to the other instances.
* Modify resolvconf on each instance to search the k8s.local domain:
    ```shell
    sudo echo "search k8s.local" >> /etc/resolvconf/resolv.conf.d/base
    sudo systemctl restart resolvconf
    ```
* setup the hostname of each instance according to the names above.