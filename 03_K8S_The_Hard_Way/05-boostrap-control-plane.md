## What is the Kubernetes Control Plane?

Set of services that control the K8S cluster.\
Control plane components make global decisions about the cluster (i.e. scheduling)
and detect and respond to cluster events (i.e. starting new pod when replication
controller's _replicas_ field is unsatisfied).


## Control Plane Components:

* **kube-api-server** - Serves the K8S API. This allows users to interact with the cluster.
* **etcd** - Cluster datastore.
* **kube-scheduler** - Schedules pods on available worker nodes.
* **kube-controller-manager** - Runs series of controllers with various functionality.
* **cloud-controller-manager** - Handles interaction with underlying cloud providers.


## Installing K8S Control Plane binaries

1. Create the `/etc/kubernetes/config` directory:\
    `sudo mkdir -p /etc/kubernetes/config`
    
2. Download the binaries for the version you want (i.e. replace `<VERSION>` with `v1.17.0`):
    ```shell
    wget https://storage.googleapis.com/kubernetes-release/release/<VERSION>/bin/linux/amd64/kube-apiserver
    wget https://storage.googleapis.com/kubernetes-release/release/<VERSION>/bin/linux/amd64/kube-controller-manager
    wget https://storage.googleapis.com/kubernetes-release/release/<VERSION>/bin/linux/amd64/kube-scheduler
    wget https://storage.googleapis.com/kubernetes-release/release/<VERSION>/bin/linux/amd64/kubectl
    ```
   
3. Make the binaries executable and copy them to `/usr/local/bin`:
    ```shell
    sudo chmod +x kube-apiserver kube-controller-manager kube-scheduler kubectl
    sudo mv kube-apiserver kube-controller-manager kube-scheduler kubectl /usr/local/bin/
    ```
   

## Seting up systemd services for the installed binaries

#### Setting up the K8S API Server

1. Create the `/var/lib/kubernetes` directory:\
`sudo mkdir -p /var/lib/kubernetes`

2. Copy the CA, API Server & Service Account keys and Data Encryption config to that dir:
    ```shell
    sudo cp ca.pem ca-key.pem \
      kubernetes-key.pem kubernetes.pem \
      service-account-key.pem service-account.pem \
      encryption-config.yaml /var/lib/kubernetes/
    ```
   
3. Setup variables to be used when generating the systemd unit file
(replace the values for each controller):
    ```shell
    INTERNAL_IP=<CONTROLLER-PRIVATE-IP>
    MASTER01_IP=<CONTROLLER-01-PRIVATE-IP>
    MASTER02_IP=<CONTROLLER-02-PRIVATE-IP>
    ```
   
4. Generate the systemd unit file for kube-api-server:
    ```shell
    cat << EOF | sudo tee /etc/systemd/system/kube-apiserver.service
    [Unit]
    Description=Kubernetes API Server
    Documentation=https://github.com/kubernetes/kubernetes
    
    [Service]
    ExecStart=/usr/local/bin/kube-apiserver \\
      --advertise-address=${INTERNAL_IP} \\
      --allow-privileged=true \\
      --apiserver-count=3 \\
      --audit-log-maxage=30 \\
      --audit-log-maxbackup=3 \\
      --audit-log-maxsize=100 \\
      --audit-log-path=/var/log/audit.log \\
      --authorization-mode=Node,RBAC \\
      --bind-address=0.0.0.0 \\
      --client-ca-file=/var/lib/kubernetes/ca.pem \\
      --enable-admission-plugins=NamespaceLifecycle,NodeRestriction,LimitRanger,ServiceAccount,DefaultStorageClass,ResourceQuota \\
      --enable-swagger-ui=true \\
      --etcd-cafile=/var/lib/kubernetes/ca.pem \\
      --etcd-certfile=/var/lib/kubernetes/kubernetes.pem \\
      --etcd-keyfile=/var/lib/kubernetes/kubernetes-key.pem \\
      --etcd-servers=https://$MASTER01_IP:2379,https://$MASTER02_IP:2379 \\
      --event-ttl=1h \\
      --experimental-encryption-provider-config=/var/lib/kubernetes/encryption-config.yaml \\
      --kubelet-certificate-authority=/var/lib/kubernetes/ca.pem \\
      --kubelet-client-certificate=/var/lib/kubernetes/kubernetes.pem \\
      --kubelet-client-key=/var/lib/kubernetes/kubernetes-key.pem \\
      --kubelet-https=true \\
      --runtime-config=api/all=true \\
      --service-account-key-file=/var/lib/kubernetes/service-account.pem \\
      --service-cluster-ip-range=10.32.0.0/24 \\
      --service-node-port-range=30000-32767 \\
      --tls-cert-file=/var/lib/kubernetes/kubernetes.pem \\
      --tls-private-key-file=/var/lib/kubernetes/kubernetes-key.pem \\
      --v=2 \\
      --kubelet-preferred-address-types=InternalIP,InternalDNS,Hostname,ExternalIP,ExternalDNS
    Restart=on-failure
    RestartSec=5
    
    [Install]
    WantedBy=multi-user.target
    EOF
    ```
   
#### Setting up the Kubernetes Controller Manager

1. Copy the kube-controller-manager kubeconfig to `/var/lib/kubernetes`:\
`sudo cp kube-controller-manager.kubeconfig /var/lib/kubernetes/`

2. Generate the systemd unit file for kube-controller-manager:
    ```shell
    cat << EOF | sudo tee /etc/systemd/system/kube-controller-manager.service
    [Unit]
    Description=Kubernetes Controller Manager
    Documentation=https://github.com/kubernetes/kubernetes
    
    [Service]
    ExecStart=/usr/local/bin/kube-controller-manager \\
      --address=0.0.0.0 \\
      --cluster-cidr=10.200.0.0/16 \\
      --cluster-name=kubernetes \\
      --cluster-signing-cert-file=/var/lib/kubernetes/ca.pem \\
      --cluster-signing-key-file=/var/lib/kubernetes/ca-key.pem \\
      --kubeconfig=/var/lib/kubernetes/kube-controller-manager.kubeconfig \\
      --leader-elect=true \\
      --root-ca-file=/var/lib/kubernetes/ca.pem \\
      --service-account-private-key-file=/var/lib/kubernetes/service-account-key.pem \\
      --service-cluster-ip-range=10.32.0.0/24 \\
      --use-service-account-credentials=true \\
      --v=2
    Restart=on-failure
    RestartSec=5
    
    [Install]
    WantedBy=multi-user.target
    EOF
    ```
   
#### Setting up the Kubernetes Scheduler

1. Copy the kube-scheduler kubeconfig to `/var/lib/kubernetes`:\
`sudo cp kube-scheduler.kubeconfig /var/lib/kubernetes/`

2. Generate the kube-scheduler yaml manifest:
    ```shell
    cat << EOF | sudo tee /etc/kubernetes/config/kube-scheduler.yml
    apiVersion: kubescheduler.config.k8s.io/v1alpha1
    kind: KubeSchedulerConfiguration
    clientConnection:
      kubeconfig: "/var/lib/kubernetes/kube-scheduler.kubeconfig"
    leaderElection:
      leaderElect: true
    EOF
    ```

3. Generate the systemd unit file for kube-scheduler:
    ```shell
    cat << EOF | sudo tee /etc/systemd/system/kube-scheduler.service
    [Unit]
    Description=Kubernetes Scheduler
    Documentation=https://github.com/kubernetes/kubernetes
    
    [Service]
    ExecStart=/usr/local/bin/kube-scheduler \\
      --config=/etc/kubernetes/config/kube-scheduler.yml \\
      --v=2
    Restart=on-failure
    RestartSec=5
    
    [Install]
    WantedBy=multi-user.target
    EOF
    ```
   
#### Enable and start all configured services

1. Reload systemd, enable and start the configured services:
```shell
sudo systemctl daemon-reload
sudo systemctl enable kube-apiserver \
  kube-controller-manager \
  kube-scheduler --now  
```

2. Verify they are in running state with `systemctl status`.

3. Use kubectl to check the component statuses:\
`kubectl get componentstatuses --kubeconfig admin.kubeconfig`


## Enable HTTP Health Checks (optional)

#### Why do we need health checks?

Health checks need to be performed against the Kubernetes API to measure
the health status of the API nodes.\
Kubernetes exposes an HTTPS endpoint to be used for such checks:

_Example (manual check):_ 
```shell
$ curl -k https://localhost:6443/healthz
ok
$
```

If you use LB that can perform HTTPS health checks you are good
(as in this guide where nginx is used for API endpoint).

However if you cannot do health checks on HTTPS (as LBs on GCP),
then you have to proxy the HTTP traffic that you will send on each
controller node in order for it to reach the HTTPS only health endpoint:


#### Setting up nginx proxies on the controller nodes

1. Install nginx:\
`sudo apt install -y nginx`

2. Create nginx configuration:
    ```shell
    cat > kubernetes.default.svc.cluster.local << EOF
    server {
      listen      80;
      server_name kubernetes.default.svc.cluster.local;
    
      location /healthz {
         proxy_pass                    https://127.0.0.1:6443/healthz;
         proxy_ssl_trusted_certificate /var/lib/kubernetes/ca.pem;
      }
    }
    EOF
    ```

3. Load the configuration to nginx:
    ```shell
    sudo mv kubernetes.default.svc.cluster.local /etc/nginx/sites-available/kubernetes.default.svc.cluster.local
    sudo ln -s /etc/nginx/sites-available/kubernetes.default.svc.cluster.local /etc/nginx/sites-enabled/
    sudo systemctl restart nginx
    sudo systemctl enable nginx
    ```
   
4. Verify:\
`curl -H "Host: kubernetes.default.svc.cluster.local" -i http://127.0.0.1/healthz`


## Setup RBAC for kubelet authorization

**RBAC** - Role Based Access Control   

The K8S API needs to have permissions to access kubelet API on each node and
perform certain tasks.

This is achieved by creating a `ClusterRole` with the necessary permissions
and assign that role to the `kubernetes` user with a `ClusterRoleBinding`:

1. Create the necessary role's manifest and apply it:
    ```shell
    cat << EOF | kubectl apply --kubeconfig admin.kubeconfig -f -
    apiVersion: rbac.authorization.k8s.io/v1beta1
    kind: ClusterRole
    metadata:
      annotations:
        rbac.authorization.kubernetes.io/autoupdate: "true"
      labels:
        kubernetes.io/bootstrapping: rbac-defaults
      name: system:kube-apiserver-to-kubelet
    rules:
      - apiGroups:
          - ""
        resources:
          - nodes/proxy
          - nodes/stats
          - nodes/log
          - nodes/spec
          - nodes/metrics
        verbs:
          - "*"
    EOF
    ```

2. Bind the role to the `kubernetes` user:
    ```shell
    cat << EOF | kubectl apply --kubeconfig admin.kubeconfig -f -
    apiVersion: rbac.authorization.k8s.io/v1beta1
    kind: ClusterRoleBinding
    metadata:
      name: system:kube-apiserver
      namespace: ""
    roleRef:
      apiGroup: rbac.authorization.k8s.io
      kind: ClusterRole
      name: system:kube-apiserver-to-kubelet
    subjects:
      - apiGroup: rbac.authorization.k8s.io
        kind: User
        name: kubernetes
    EOF
    ```


## Setting up a Kube API Frontend Load Balancer

Used for redundancy (load balancing the traffic to the K8S API).

On the dedicated nginx instance do:

1. Install nginx:
    ```shell
    sudo apt-get install -y nginx
    sudo systemctl enable nginx --now
    sudo mkdir -p /etc/nginx/tcpconf.d
    ```

2. Append this to the end of `/etc/nginx/nginx.conf`:\
`include /etc/nginx/tcpconf.d/*;`

2. Setup these variables (replace with actual IPs):
    ```shell
    MASTER01_IP=<CONTROLLER-01-PRIVATE-IP>
    MASTER02_IP=<CONTROLLER-02-PRIVATE-IP>
    ```

3. Add the LB configuration and reload nginx:
    ```shell
    cat << EOF | sudo tee /etc/nginx/tcpconf.d/kubernetes.conf
    stream {
        upstream kubernetes {
            server $MASTER01_IP:6443;
            server $MASTER02_IP:6443;
        }
    
        server {
            listen 6443;
            listen 443;
            proxy_pass kubernetes;
        }
    }
    EOF
    ```

4. Reload nginx and verify (you should get cluster info):
    ```shell
    sudo nginx -s reload
    curl -k https://localhost:6443/version
    ```
   