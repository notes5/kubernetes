## What is etcd? 

[Etcd](https://etcd.io/) is open-source distributed key value store developed by CoreOS
 so that we have a reliable way to store data across a cluster of machines.\
 It makes sure the data is synchronized across all machines. 

K8S uses etcd to store the cluster configuration and state but etcd is not k8S specific or exclusive.\
It is used in other products and technologies as well.

## Creating etcd cluster

1.  Download etcd from <https://github.com/etcd-io/etcd/releases>  and install it following their guide
 (run it as bash script).
 
    Example below is the guide for version v3.4.7:

    ```shell
    ETCD_VER=v3.4.7 
    
    # choose either URL 
    GOOGLE_URL=https://storage.googleapis.com/etcd 
    GITHUB_URL=https://github.com/etcd-io/etcd/releases/download 
    DOWNLOAD_URL=${GOOGLE_URL} 
    
    rm -f /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz 
    rm -rf /tmp/etcd-download-test && mkdir -p /tmp/etcd-download-test 
    
    curl -L ${DOWNLOAD_URL}/${ETCD_VER}/etcd-${ETCD_VER}-linux-amd64.tar.gz -o /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz 
    tar xzvf /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz -C /tmp/etcd-download-test --strip-components=1 
    rm -f /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz 
    
    /tmp/etcd-download-test/etcd --version 
    /tmp/etcd-download-test/etcdctl version 
    ```

2. Move the necessary stuff to `/usr/local/bin`:\
    `sudo mv /tmp/etcd-download-test/etcd* /usr/local/bin/`
    
3. Create the necessary directories:\
    `sudo mkdir -p /etc/etcd /var/lib/etcd`

4. Copy CA's public key and API server's keys (that we uploaded previously with scp) to `/etc/etcd`:\
    `sudo cp ca.pem kubernetes-key.pem kubernetes.pem /etc/etcd/`
    
5. Setup some variables to use for the systemd unit file:
    ```shell
    ETCD_NAME=<MASTER-HOSTNAME> 
    INTERNAL_IP=<MASTER-IP> 
    INITIAL_CLUSTER=<MASTER1-HOSTNAME>=https://<MASTER1-IP>:2380,<MASTER2-HOSTNAME>=https://<MASTER2-IP>:2380
    ```
   
6. Create systemd unit file for the etcd service:
    ```shell
    cat << EOF | sudo tee /etc/systemd/system/etcd.service
    [Unit]
    Description=etcd
    Documentation=https://github.com/etcd-io/etcd
    
    [Service]
    ExecStart=/usr/local/bin/etcd \
     --name ${ETCD_NAME} \
     --cert-file=/etc/etcd/kubernetes.pem \
     --key-file=/etc/etcd/kubernetes-key.pem \
     --peer-cert-file=/etc/etcd/kubernetes.pem \
     --peer-key-file=/etc/etcd/kubernetes-key.pem \
     --trusted-ca-file=/etc/etcd/ca.pem \
     --peer-trusted-ca-file=/etc/etcd/ca.pem \
     --peer-client-cert-auth \
     --client-cert-auth \
     --initial-advertise-peer-urls https://${INTERNAL_IP}:2380 \
     --listen-peer-urls https://${INTERNAL_IP}:2380 \
     --listen-client-urls https://${INTERNAL_IP}:2379,https://127.0.0.1:2379 \
     --advertise-client-urls https://${INTERNAL_IP}:2379 \
     --initial-cluster-token etcd-cluster-0 \
     --initial-cluster ${INITIAL_CLUSTER} \
     --initial-cluster-state new \
     --data-dir=/var/lib/etcd
    Restart=on-failure 
    RestartSec=5 
    
    [Install]
    WantedBy=multi-user.target 
    EOF 
    ```
   
7. Reload systemd, enable and start the etcd service

8. Check if everything is ok:
    ```shell
    sudo ETCDCTL_API=3 etcdctl member list \
    --endpoints=https://127.0.0.1:2379 \
    --cacert=/etc/etcd/ca.pem \
    --cert=/etc/etcd/kubernetes.pem \
    --key=/etc/etcd/kubernetes-key.pem
    ```
   
   _**NOTE:**_ If you get `(error "remote error: tls: bad certificate", ServerName "")` from etcd, 
   make sure that the API server key is having the proper SAN set with this command:\
    `openssl x509 -noout -text -in /etc/etcd/kubernetes.pem | grep -A1 "Subject Alternative Name"`
    
    _Example for correct setup:_
    ```shell
    $ openssl x509 -noout -text -in /etc/etcd/kubernetes.pem | grep -A1 "Subject Alternative Name" 
        X509v3 Subject Alternative Name: 
          DNS:master01.k8s.local, DNS:master02.k8s.local, DNS:nginx.k8s.local, DNS:localhost, DNS:kubernetes.default, IP Address:10.32.0.1, IP Address:172.31.19.192, IP Address:172.31.30.20, IP Address:172.31.28.134, IP Address:127.0.0.1 
    $ 
    ```
