## Setup kubectl

According to the diagram kubectl must be setup to:

* operate against the cluster remotely (bastion.k8s.local)
by accessing the API load balancer (nginx.k8s.local)
* use kubeconfig for the admin user 
(so we can have admin rights the configure the cluster)

#### Steps:

1. Login to the bastion instance and setup variable 
to hold the private ip of the API LB instance:\
`LB_IP="172.31.28.134"`

2. Generate the kubeconfig to be used by kubectl:
    ```shell
    kubectl config set-cluster kthw \
      --certificate-authority=ca.pem \
      --embed-certs=true \
      --server=https://$LB_IP:6443
    
    kubectl config set-credentials admin \
      --client-certificate=admin.pem \
      --client-key=admin-key.pem
    
    kubectl config set-context kthw \
      --cluster=kthw \
      --user=admin
    
    kubectl config use-context kthw
    ```

3. Setup kubectl bash completion (optional):
    ```shell
    sudo apt install bash-completion
    sudo bash -c 'kubectl completion bash > /etc/bash_completion.d/kubectl'
    ```

4. Verify:
    ```shell
    kubectl get nodes
    kubectl version
    ```