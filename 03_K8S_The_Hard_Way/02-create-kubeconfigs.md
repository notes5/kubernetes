## What is kubeconfig and why do we need them? 

Kubernetes configuration file that stores information about clusters, users, 
namespaces and authentication mechanisms = Contains the configuration needed to 
connect and interact with a cluster. 

It is comprised of one or more contexts which contain: 

-   The location of the cluster you want to connect to 
-   What user you want to authenticate as 
-   Data needed for authentication (client certificate or token) 

By using different contexts in kubeconfig you can easily switch between different clusters. 

Kubeconfigs are used also by cluster components like kubelet so that
 they know the location (IP) of the K8S API (server) and how to authenticate with it.
 
## Generating kubeconfigs for the cluster: 

1.  Create variable to store the IP of the nginx load balancer instance:\
    `export K8S_ADDRESS=<load balancer private ip>`

2.  Generate kubelet kubeconfig for each worker node (run this as bash script):
    ```shell
    for instance in <NODE1-HOSTNAME> <NODDE2-HOSTNAME>; do 
      kubectl config set-cluster <CLUSTER-NAME> \ 
        --certificate-authority=ca.pem \ 
        --embed-certs=true \ 
        --server=https://${K8S_ADDRESS}:6443 \ 
        --kubeconfig=${instance}.kubeconfig 
    
      kubectl config set-credentials system:node:${instance} \ 
        --client-certificate=${instance}.pem \ 
        --client-key=${instance}-key.pem \ 
        --embed-certs=true \ 
        --kubeconfig=${instance}.kubeconfig 
     
      kubectl config set-context default \ 
        --cluster=<CLUSTER-NAME> \ 
        --user=system:node:${instance} \ 
        --kubeconfig=${instance}.kubeconfig 
    
      kubectl config use-context default --kubeconfig=${instance}.kubeconfig 
    done
    ```
    
3.  Generate kube-proxy kubeconfig (run as bash script):
    ```shell
    kubectl config set-cluster <CLUSTER-NAME> \ 
        --certificate-authority=ca.pem \ 
        --embed-certs=true \ 
        --server=https://${K8S_ADDRESS}:6443 \ 
        --kubeconfig=kube-proxy.kubeconfig 
    
    kubectl config set-credentials system:kube-proxy \ 
        --client-certificate=kube-proxy.pem \ 
        --client-key=kube-proxy-key.pem \ 
        --embed-certs=true \ 
        --kubeconfig=kube-proxy.kubeconfig 
    
    kubectl config set-context default \ 
        --cluster=<CLUSTER-NAME> \ 
        --user=system:kube-proxy \ 
        --kubeconfig=kube-proxy.kubeconfig 
    
    kubectl config use-context default --kubeconfig=kube-proxy.kubeconfig
    ```

4.  Generate kube-controller-manager kubeconfig (run as bash script):
    ```shell
    kubectl config set-cluster <CLUSTER-NAME> \ 
        --certificate-authority=ca.pem \ 
        --embed-certs=true \ 
        --server=https://127.0.0.1:6443 \ 
        --kubeconfig=kube-controller-manager.kubeconfig 
    
    kubectl config set-credentials system:kube-controller-manager \ 
        --client-certificate=kube-controller-manager.pem \ 
        --client-key=kube-controller-manager-key.pem \ 
        --embed-certs=true \ 
        --kubeconfig=kube-controller-manager.kubeconfig 
    
    kubectl config set-context default \ 
        --cluster=<CLUSTER-NAME> \ 
        --user=system:kube-controller-manager \ 
        --kubeconfig=kube-controller-manager.kubeconfig 
    
    kubectl config use-context default --kubeconfig=kube-controller-manager.kubeconfig 
    ```

5.  Generate kube-scheduler kubeconfig (run as bash script):
    ```shell
    kubectl config set-cluster <CLUSTER-NAME> \ 
        --certificate-authority=ca.pem \ 
        --embed-certs=true \ 
        --server=https://127.0.0.1:6443 \ 
        --kubeconfig=kube-scheduler.kubeconfig 
    
    kubectl config set-credentials system:kube-scheduler \ 
        --client-certificate=kube-scheduler.pem \ 
        --client-key=kube-scheduler-key.pem \ 
        --embed-certs=true \ 
        --kubeconfig=kube-scheduler.kubeconfig 
     
    kubectl config set-context default \ 
        --cluster=<CLUSTER-NAME> \ 
        --user=system:kube-scheduler \ 
        --kubeconfig=kube-scheduler.kubeconfig 
     
    kubectl config use-context default --kubeconfig=kube-scheduler.kubeconfig
    ```

6.  Generate admin kubeconfig (run as bash script):
    ```shell
    kubectl config set-cluster <CLUSTER-NAME> \ 
        --certificate-authority=ca.pem \ 
        --embed-certs=true \ 
        --server=https://127.0.0.1:6443 \ 
        --kubeconfig=admin.kubeconfig 
    
    kubectl config set-credentials admin \ 
        --client-certificate=admin.pem \ 
        --client-key=admin-key.pem \ 
        --embed-certs=true \ 
        --kubeconfig=admin.kubeconfig 
    
    kubectl config set-context default \ 
        --cluster=<CLUSTER-NAME> \ 
        --user=admin \ 
        --kubeconfig=admin.kubeconfig 
    
    kubectl config use-context default --kubeconfig=admin.kubeconfig 
    ```

## Distribute the kubeconfigs to the cluster nodes:

1.  Copy the kubelet and kube-proxy kubeconfigs to the worker nodes:\
    `scp <NODE-HOSTNAME>.kubeconfig kube-proxy.kubeconfig <USER>@<NODE>:~`

1.  Copy the admin, kube-controller-manager and kube-scheduler kubeconfigs
 to the controller nodes:\
    `scp admin.kubeconfig kube-controller-manager.kubeconfig kube-scheduler.kubeconfig <USER>@<MASTER>:~`


###### Command Reference
| Command | Description |
| :---    | ---:        |
| `kubectl config set-cluster`         | Sets up the configuration for the location of the cluster            | 
| `kubectl config set-credentials`     | Sets the username & client certificate to be used for authentication |
| `kubectl config set-context default` | Sets up the default context                                          | 
| `kubectl config use-context default` | Sets the current context to the one we just configured               |
