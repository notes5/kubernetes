## Docker vs K8S Networking Models

#### Docker Networking Model

![docker-net-model](images/docker-net-model.png)

Provisions virtual network bridge interface on the host
where containers are run and they talk to each other through it.

However the docker model has limitations:
* you have to proxy the traffic if containers from one host need
to connect to containers on different host
(one proxy for each pair of containers)
* make sure ports used by containers on a host do not overlap

#### K8S Networking Model

![k8s-net-model](images/k8s-net-model.png)

Steps on the docker networking model but was designed to 
overcome the limitations of the docker model.

Creates:
* One virtual network for the whole cluster (instead one per host) - 
pods can talk to each other (even on different nodes) and have
unique IP within the cluster
* Services also have unique IP (from different range than pods) - 
pods can talk to services


## Cluster Network Architecture

#### Important CIDR ranges
* **Cluster CIDR** - Used to assign IPs to pods (i.e. 10.200.0.0/16)
* **Service Cluster IP Range** - Used to assign IPs for services 
(i.e. 10.32.0.0/24)
* **Pod CIDR** - IP Range for pods on a specific worker node. This
range should fall under the Cluster CIDR, but not overlap with the
Pod CIDR of any other worker node. \
_**NOTE:**_ This is not manual work - the networking plugin (i.e. flannel) 
handles node IP allocation automatically.


## Installing network plugin (Weave Net)

1. Make sure that `net.ipv4.conf.all.forwarding = 1` in sysctl
on each worker node:
    ```shell
    ### Check
    sudo sysctl -a | grep net.ipv4.conf.all.forwarding
    
    ### Setup if not equals one
    sudo sysctl net.ipv4.conf.all.forwarding=1
    echo "net.ipv4.conf.all.forwarding=1" | sudo tee -a /etc/sysctl.conf
    ```

2. Install Weave Net for the Cluster CIDR range 
from where kubectl was configured for remote work 
(i.e. bastion.k8s.local): \
`kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"&env.IPALLOC_RANGE=10.200.0.0/16"`

3. Verify / Test the Weave Net network
```shell
###  Check if weavenet pod is provisioned on each worker node
kubectl get pods -n kube-system

### Create sample nginx deployment and expose it as service
cat << EOF | kubectl apply -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx
spec:
  selector:
    matchLabels:
      run: nginx
  replicas: 2
  template:
    metadata:
      labels:
        run: nginx
    spec:
      containers:
      - name: my-nginx
        image: nginx
        ports:
        - containerPort: 80
EOF

kubectl expose deployment nginx

### check if nginx service and pod IPs (ranges) overlap
kubectl get ep nginx
kubectl get svc nginx

### start session in busybox pod with curl):
kubectl run curl --image=radial/busyboxplus:curl -i --tty

### in that session curl the IPs of the pods and the services

## cleanup the test resources
kubectl delete svc nginx
kubectl delete deployment nginx
kubectl delete deployment curl
```