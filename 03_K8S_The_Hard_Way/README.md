# Content

## Table of Contents

* [00 - Course Resources](00-course-resources.md)
* [01 - Create CA & TLS Certs](01-create-ca-and-tls-certs.md)
* [02 - Create kubeconfigs](02-create-kubeconfigs.md)
* [03 - Setup Data Encryption](03-setup-data-encryption.md)
* [04 - Bootstrap etcd](04-bootstrap-etcd.md)
* [05 - Bootstrap The Control Plane](05-boostrap-control-plane.md)
* [06 - Bootsrap The Worker Nodes](06-bootsrap-worker-nodes.md)
* [07 - Config kubectl](07-config-kubectl.md)
* [08 - Networking](08-networking.md)
* [09 - Deploy DNS Add-on](09-deploy-dns-addon.md)
* [10 - Smoke Testing](10-smoke-testing.md)