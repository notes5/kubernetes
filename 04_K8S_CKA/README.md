# Content

* [01 - Core Concepts (19%)](01-core-concepts.md)
* [02 - Installation, Configuration, and Validation (12%)](02-install-config-validate.md)
* [03 - Cluster Maintenance (11%)](03-cluster.md)
* [04 - Networking (11%)](04-networking.md)

#### CKA Cheat Sheet (useful commands):

https://github.com/nkuba/k8s-admin-helper

#### CKA Handbook:

<https://training.linuxfoundation.org/wp-content/uploads/2020/04/CKA-CKAD-Candidate-Handbook-v1.10.pdf>

#### CKA Curriculum:

<https://github.com/cncf/curriculum>
