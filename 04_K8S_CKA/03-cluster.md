# Managing the Kubernetes Cluster

## Upgrading the cluster

kubeadm allows us to upgrade our cluster components in the proper order, 
making sure to include important feature upgrades we might want to
take advantage of in the latest stable version of Kubernetes.

Steps:

1. Check current K8S version (different components can be
at different versions but the API version is the actual one): 
    ```shell
    ### Check API server's version:
    kubectl version short
    
    ### Check version of kubelet:
    kubectl describe nodes | grep "Kubelet Version"
    
    ### Check version of kube-proxy:
    kubectl describe nodes | grep "Kube-Proxy Version"
    
    ### Check kube-controller-manager's version
    kubectl -n kube-system get pods | grep kube-controller-manager
    kubectl -n kube-system describe <POD-NAME> | grep Image
    ```

2. Unlock the packages back so that 
apt can update them (on each node): \
`sudo apt-mark unhold kubeadm kubectl kubelet`
 
3. Check what versions are available, install and hold the
desired version of kubeadm (on each node):
    ```shell
    sudo apt-cache madison kubeadm
    sudo apt install -y kubeadm=<VERSION>
    sudo apt-mark hold kubeadm
    ```

4. Plan (and then upgrade) the controller components:
    ```shell
    sudo kubeadm upgrade plan
    sudo kubeadm upgrade apply <VERSION>
    ```

5. Update and then hold kubectl on each node:
    ```shell
    sudo apt install -y kubectl=<VERSION>
    sudo apt-mark hold kubectl
    ```   

6. Update and then hold kubelet on each node:
    ```shell
    sudo apt install -y kubelet=<VERSION>
    sudo apt-mark hold kubelet
    ```   


## OS upgrades within a cluster

When we need to take a node down for maintenance, Kubernetes makes it
easy to evict the pods on that node, take it down, and then continue
scheduling pods after the maintenance is complete.

Furthermore, if the node needs to be decommissioned, you can just
as easily remove the node and replace it with a new one, 
joining it to the cluster.

Steps:

1. Evict the pods from a node and confirm the result
(this **cordones** the node, disabling scheduling of pods on it): \
    ```shell
    kubectl drain <NODE-NAME> --ignore-daemonsets
    kubectl get nodes
    ```

2. Take down the node for maintenance, patch, etc.

3. **Uncordon** the node so that pods can be scheduled on it again: \
`kubectl uncordon <NODE-NAME>`

**NOTES**: 
* If you want to remove the node altogether after it is drained
run: \
 `kubectl delete node <NODE-NAME>`

* If you want to join it back to the cluster run:
    ```shell
    ### Generate new token from the master node:
    sudo kubeadm token generate
    
    ### Get the join command to run on the node to add it back:
    sudo kubeadm token create <TOKEN-NAME> --ttl 2h --print-join-command 
    ```
     
    
## Backup / Restore the cluster

#### Backup etcd

etcd should be backed up as follows:

1. Download the etcd binaries (adjust with the desired version
as seen on https://github.com/etcd-io/etcd/releases) and
move them to /usr/local/bin:
    ```shell
    VERSION="v3.4.3"
    wget https://github.com/etcd-io/etcd/releases/download/$VERSION/etcd-$VERSION-linux-amd64.tar.gz
    tar xvf etcd-$VERSION-linux-amd64.tar.gz
    sudo mv etcd-$VERSION-linux-amd64/etcd* /usr/local/bin
    ```
   
2. Note the names of the CA and the server certificates that we
need to provide when running the etcdctl snapshot save command: \
`ls -l /etc/kubernetes/pki/etcd/` 

3. Snapshot the etcd state as backup:
    ```shell
    sudo ETCDCTL_API=3 etcdctl snapshot save snapshot.db \
      --cacert /etc/kubernetes/pki/etcd/ca.crt \
      --cert /etc/kubernetes/pki/etcd/server.crt \
      --key /etc/kubernetes/pki/etcd/server.key
    ```  
   
4. Backing up the etcd certificates: \
`sudo tar -zcvf etcd_certs.tar.gz /etc/kubernetes/pki/etcd`

5. You can then copy the two files somewhere else as backup: \
`scp snapshot.db etcd_certs.tar.gz <USER>@<DESTINATION>:<PATH>`

#### Restore etcd (notes)

`ETCDCTL_API=3 etcdctl snapshot restore snapshot.db [OPTIONS]`

Restoring overwrites some snapshot metadata (specifically, 
the member ID and cluster ID); the member loses its former identity.

This metadata overwrite prevents the new member from inadvertently 
joining an existing cluster. Therefore in order to start a cluster
from a snapshot, the restore must start a new logical cluster.

