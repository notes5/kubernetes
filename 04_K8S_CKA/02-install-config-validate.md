# Building the Kubernetes Cluster

## Release Binaries, Provisioning, and Types of Clusters

### Picking the right solution

* Custom-built Kubernetes:
    * Install manually
    * Configure your own network fabric
    * Locate the Release binaries
    * Build your own images
    * Secure cluster communication
    
* Pre-built Kubernetes:
    * Minikube
    * Minishift
    * MicroK8s
    * Ubuntu on LXD
    * AWS / Azure / GCP
    
##### Command reference

| Command | Description |
| :---    | ---:        |
| `kubectl cluster-info`             | View address of master and services | 
| `kubectl config view`              | Show kubeconfig settings            |
| `kubectl describe nodes`           | Show all nodes' details             | 
| `kubectl describe pods`            | Show all pods' details              | 
| `kubectl get svc --all-namespaces` | Show all services                   |
| `kubectl api-resources -o wide`    | View all resources                  |


## Installing K8S with kubeadm on Ubuntu 16.04

Setup 3-node cluster (1 master and 2 workers):

Execute on each node (unless explicitly specified otherwise):

1. Add Docker's repository:
    ```shell
    ### Add Docker's GPG key
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    
    ### Add the repository
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) stable"
    ```

2. Add K8S' repository:
    ```shell
    ### Add K8S' GPG key
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
    
    ### Add the repository
    cat << EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
    deb https://apt.kubernetes.io/ kubernetes-xenial main
    EOF
    ```

3. Install the necessary binaries:
    ```shell
    ### Update apt's cache
    sudo apt-get update
    
    ### Install latest docker and the K8S binaries for v1.17
    sudo apt-get install -y \
      docker-ce \
      kubelet=1.17.0-00 \
      kubeadm=1.17.0-00 \
      kubectl=1.17.0-00
    
    ### Stop apt from updating the binaries:
    sudo apt-mark hold docker-ce kubelet kubeadm kubectl
    ```

4. Configuring the instances' iptables:
    ```shell
    echo "net.bridge.bridge-nf-call-iptables=1" | sudo tee -a /etc/sysctl.conf
    sudo sysctl -p
    ```
   
5. Init the cluster (run on the master node): \
`sudo kubeadm init --pod-network-cidr=10.244.0.0/16`

    _**NOTE:**_ copy the join hash from the command's output!

6. Setup local kubeconfig (run on the master node):
    ```shell
    mkdir -p $HOME/.kube
    sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
    sudo chown $(id -u):$(id -g) $HOME/.kube/config
    ```
   
7. Install flannel as network plugin (run on the master node): \
`kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml`

8. Join the worker nodes using the hash from step 5
(run on each worker node): \
`sudo kubeadm join <HASH>`

9. Check if all nodes show up (run on the master node): \
`kubectl get nodes`
 
 
## Build HA cluster

You can provide high availability for cluster components by running
 multiple instances — however, some replicated components must remain 
 in standby mode. \
The scheduler and the controller manager are 
 actively watching the cluster state and take action when it changes. \
If multiples are running, it creates the possibility of unwarranted 
 duplicates of pods:
 
![ha-cluster](images/k8s-ha-cluster.png)

Controlling whether the applicable components on each controller node
are active or standby happens via the **Leader Elect** option.

_Example:_

_command:_ \
`kubectl get endpoints kube-scheduler -n kube-system -o yaml`

_output:_
```yaml
apiVersion: v1
kind: Endpoints
metadata:
  annotations:
    control-plane.alpha.kubernetes.io/leader: '{"holderIdentity":"master01.k8s.local_67f766fd-27f0-4bc9-922f-815f7ab90a5a","leaseDurationSeconds":15,"acquireTime":"2020-04-24T11:20:52Z","renewTime":"2020-04-24T11:59:58Z","leaderTransitions":0}'
  creationTimestamp: "2020-04-24T11:20:52Z"
  name: kube-scheduler
  namespace: kube-system
  resourceVersion: "6121"
  selfLink: /api/v1/namespaces/kube-system/endpoints/kube-scheduler
  uid: 3489be8a-b6d9-48f5-a1df-bc79ffa166ef
```

As visible the **leader** `kube-scheduler` is on the 
master01.k8s.local controller node.

### Replicating etcd

etcd is cluster-ready datastore that can operate simultaneously
(i.e. unlike kube-scheduler). \
As usual for cluster architectures etcd works with quorum so you
have to deploy odd number of etcd instances to secure a majority 
quorum (more than 50%).

etcd instances can use 2 kinds of topologies:
* **stacked** - Each controller created local etcd that communicates 
only with that controller s API server endpoint.
* **external** - Not deployed in the cluster at all (as pods but
as let's say systemd service on the OS). 

You can provision stacked etcd topology following this guide: \

1. Setup Load Balancer as single API server endpoint.

2. Install etcd on each controller node
(download binaries, configure systemd service, etc.)

3. Create kubeadm-config.yaml file with the following content 
(replacing the needed variables (in CAPS) with actual values):
    ```yaml
    apiVersion: kubeadm.k8s.io/v1beta2
    kind: ClusterConfiguration
    kubernetesVersion: stable
    controlPlaneEndpoint: "LOAD_BALANCER_DNS:LOAD_BALANCER_PORT"
    etcd:
        external:
            endpoints:
            - https://ETCD_0_IP:2379
            - https://ETCD_1_IP:2379
            - https://ETCD_2_IP:2379
            caFile: /etc/kubernetes/pki/etcd/ca.crt
            certFile: /etc/kubernetes/pki/apiserver-etcd-client.crt
            keyFile: /etc/kubernetes/pki/apiserver-etcd-client.key
    ```
   
4. Create a stacked etcd topology using kubeadm: \
`kubeadm init --config=kubeadm-config.yaml` 

Reference: <https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/high-availability/>


## Securing Cluster's Communications

![k8s-api-access](images/k8s-api-access-chain.png)

The K8S API server provides CRUD (Create, Read, Update, Delete) interface
for querying and modifying the cluster state via RESTful API.

To prevent unauthorized users from modifying the cluster state, RBAC is used, 
defining roles and role bindings for a user.

![k8s-rbac](images/k8s-rbac.png)

RBAC is managed by 4 resources (in 2 logical groups):

* Resources per namespace:
    * Role 
    * RoleBinding
* Cluster-wide resources:     
    * ClusterRole
    * ClusterRoleBinding

Roles define what can be done; \
RoleBindings define who can do it;

_Example:_

The default service account will not allow you to 
list the services in a namespace. 

```shell
### Create namespace
kubeclt craete ns my-ns

### Run kube-proxy pod in that namespace
kubectl run test --image=chadmcrowell/kubectl-proxy -n my-ns

### Open session from the pod
kubectl -n my-ns get pods
kubectl -n my-ns exec -it <POD-NAME> sh
```

_result:_
```shell
ubuntu@master01:~$ kubectl -n my-ns get pods
NAME                    READY   STATUS    RESTARTS   AGE
test-659588d944-g4l9g   1/1     Running   0          23s
ubuntu@master01:~$ kubectl -n my-ns exec -it test-659588d944-g4l9g sh
/ # curl localhost:8001/api/v1/namespaces/my-ns/services
{
  "kind": "Status",
  "apiVersion": "v1",
  "metadata": {

  },
  "status": "Failure",
  "message": "services is forbidden: User \"system:serviceaccount:my-ns:default\" cannot list resource \"services\" in API group \"\" in the namespace \"my-ns\"",
  "reason": "Forbidden",
  "details": {
    "kind": "services"
  },
  "code": 403
}/ #
```

_**NOTE:**_ You can check the token you use to authenticate as a
service account in a pod by executing (from the pod): \
`cat /var/run/secrets/kubernetes.io/serviceaccount/token`

##### Command Reference

| Command | Description |
| :---    | ---:        |
| `cat .kube/config`            | View the kubeconfig                | 
| `kubectl get secrets`         | View the service account token     |
| `kubectl get serviceaccounts` | List the service account resources |


##  Testing the cluster

Check the following (example with nginx):

* Deployments can run:
    ```shell
    kubectl run nginx --image=nginx
    kubectl get deployments
    ```

* Pods can run: \
`kubectl get pods`

* Pods can be directly accessed: 
    ```shell
    kubectl port-forward <POD-NAME> 8081:80
    curl --head http://localhost:8081
    ```

* Logs can be collected: \
`kubectl logs <POD-NAME>`

* Commands run from pod: \
`kubectl exec -it <POD-NAME> -- nginx -v`

* Services can provide access:
    ```shell
    kubectl expose deployment nginx --port 80 --type NodePort
    kubectl get svc
    curl localhost:<NodePort>
    ```
  
* Nodes and pods are healthy:
    ```shell
    kubectl get nodes
    kubectl get pods
    kubectl describe nodes
    kubectl describe pods
    ```


  