# Understanding Kubernetes Architecture

## Cluster Architecture

![Cluster Architecture](images/k8s-architecture.png)

### Master  

-   Can be 1 or more. 

-   The control plane. 

-   Will NOT run application components. 

-   Can be replicated for HA 

#### Components:

-   **API server** - Communication hub for all cluster components 

-   **Scheduler** - Assigns application to a worker node. Auto detects which pod to 
assign to which node based on available resources, constraints, etc. 

-   **Controller Manager** - Maintains the cluster. Handles node failures, replicating
 components, maintaining the correct amount of pods, etc. 

-   **etcd** - Data store that stores the cluster's configuration. 

### Worker 

-   Can be 0 or more. 

-   Handles the actual workload. 

#### Components: 

-   **kubelet** - Runs / Manages containers on a node. 

-   **kube-proxy** - Load balances traffic between application components. 

-   **Container runtime** - The program that runs your containers (docker, rkt, containerd, etc.)

![Applicaion on K8S](images/k8s-app.png)

##### Command Reference

| Command | Description |
| :---    | ---:        |
| `kubectl get nodes`                           | Lists all nodes in the cluster            | 
| `kubectl get pods --all-namespaces`           | Lists pods in all namespaces              |
| `kubectl get pods --all-namespaces -o wide`   | Lists all pods in the cluster in detail   | 
| `kubectl get namespaces`                      | Show all the namespace names              | 
| `kubectl describe pod <pod-name>`             | All details about a pod                   | 
| `kubectl delete pod <pod-name>`               | Deletes a pod                             | 

## API Primitives

All cluster components talk to the API server only! \
Only the API server talks to the etcd data store! 

kubectl processes the needed API calls. This API calls can be described in human-readable 
format as YAML files (called manifests) which we can use to specify and customize our request
 and then pass to kubectl. 
 
 _Example:_
 
 ```yaml
apiVersion: apps/v1 
kind: Deployment 
metadata: 
  name: nginx-deployment 
spec: 
  selector: 
    matchLabels: 
      app: nginx 
  replicas: 2 
  template: 
    metadata: 
      labels: 
        app: nginx 
    spec: 
      containers: 
      - name: nginx 
        image: nginx:1.7.9 
        ports: 
        - containerPort: 80 
```

### Key API primitives: 

-   **apiVersion** - K8S API version that indicates the API path. 
Used to ensure the API presents a clear, consistent view of system resources and behavior 
-   **kind** - The kind of object you want to create 
-   **metadata** - Data to makes an object uniquely identifiable (including 
name string, UID, optional namespace, etc.) 
    -   **labels** - Key/Value pairs to uniquely identify an object for selector purposes. 
    -   **annotations** - Key/Value pairs (like labels) but cannot be used by selectors
     as criteria to choose an object for an action = They do not hold identifying information
      = You cannot delete pods based on an annotation, but can do that based on label. 
-   **spec** - Describes the desired state of the object and characteristics you want the object to have. 
    -   **template** 
        -   **spec** - Describes the pod container's image, volumes, exposed ports, etc. 
-   **status** - Describes the actual state of the object and is supplied/updated by K8S. 
Used to align the actual state to the desired one. => The spec should always matches the status!

##### Command Reference

| Command | Description |
| :---    | ---:        |
| `kubectl get componentstatus [<component-name>]`                             | Shows the health of the K8S components (or one of them) | 
| `kubectl get deployment <deployment-name> -o yaml`                           | Gets the full YAML of a deployment object back          |
| `kubectl get pods --show-labels`                                             | Show all pod labels for the default namespace           | 
| `kubectl label pods <pod-name> <label-key>=<label-value>`                    | Applies label to a pod                                  | 
| `kubectl get pods -L <label-key>`                                            | See specific labels as columns in the output            | 
| `kubectl annotate deployment <deployment-name> <key>=<value>`                | Annotates a deployment object                           | 
| `kubectl get pods --field-selector <selector>=<value>[,<selector2>=<value>]` | Use field selector(s) to filter objects when querying   |

## Service & Network Primitives

![K8S Service](images/k8s-service.png)

Service objects allow you to dynamically access groups of replicas pods. 

Normally they are selected via label.\
This is the way to expose (hence access) something running in K8S either within the cluster or externally. 

_Example:_

```yaml
apiVersion: v1 
kind: Service 
metadata: 
  name: nginx-nodeport 
spec: 
  type: NodePort 
  ports: 
  - protocol: TCP 
    port: 8080 
    targetPort: 80 
    nodePort: 30080 
  selector: 
    app: nginx 
```

### About service's ports settings: 

-   **nodePort** - port to access the service from outside the cluster (curl from a node is also "outside") 
-   **port** - port to access the service from inside the cluster  (curl the service's IP) 
-   **targetPort** - the port on the pod that requests to nodePort or port get routed to. 

_Example with the above YAML:_

External requests go to 30080 (nodePort) -> routed to 8080 (port) -> sent to 80 (targetPort).\
Normally you would want port to be equal to targetPort.

### kube-proxy's role in services:

![K8S kube-proxy](images/k8s-kube-proxy.png)

When service is created a virtual NIC is set up and the API server makes kube-proxy aware of this.\
Kube-proxy then updates the iptables rules on each node accordingly.\
So if a service (pod) want to connect to another service (pod) it sends traffic to the service's virtual IP.\
When it reaches iptables' rules the packet's destination changes to the other pod's actual IP
 according to the populated rules.
 
##### Command Reference 
 
| Command | Description |
| :---    | ---:        |
| `kubectl get pods -o wide`             | Show all pods along with their IP addresses          | 
| `kubectl create -f <service.yml>`      | Creates a service from YAML manifest                 |
| `kubectl get svc [<service-name>]`     | Shows services (or specific one if name is supplied) | 
| `curl localhost:<node-port>`           | Access a service through a node port                 | 
| `kubectl exec <pod-name> -- <command>` | Execute command from a pod                           |


## LAB Notes:

1. Lists all nodes / pods / deployments / namespaces:
    ```shell
    kubectl get nodes
    kubectl get pods --all-namespaces
    kubectl get deployments --all-namespaces
    kubectl get ns
    ```
   
2. Get the apiserver's IP(s) (from its pods): \
`kubectl get pods -n kube-system -o wide | grep apiserver`

3. Show the labels for the pods (in all namespaces): \
`kubectl get pods --all-namespaces --show-labels -o wide`

