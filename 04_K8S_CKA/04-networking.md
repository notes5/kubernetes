# Cluster Communications

## Prerequisites:

Run sample nginx deployment: \
`kubectl apply -f https://k8s.io/examples/application/deployment.yaml`

## Pod & Node Networking

#### Networking inside a node

A Virtual interface pair is created for the container:
* one for the node's namespace (`vethXXX`)
* one for the container's network namespace (`eth0@if<NUMBER>`)

![intra-node-networking](images/k8s-pod-node-networking.png)

For every container in a pod running on a node a virtual interface
starting with `veth` is created on the node.

If you check the network interface on container level you will see
that it's named eth0@if<NUMBER> where the number is which interface
on the host it is mapped to.

_**Example:**_

```shell
### Login to the node and check the interfaces (output shortened):
ubuntu@node01:~$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
2: ens5: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 9001 qdisc mq state UP group default qlen 1000
3: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default
4: flannel.1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8951 qdisc noqueue state UNKNOWN group default
5: cni0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8951 qdisc noqueue state UP group default qlen 1000
12: vethc5fd7357@if3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8951 qdisc noqueue master cni0 state UP group default

### Find the container name and PID:
ubuntu@node01:~$ sudo docker ps | grep nginx
5b68dd0c5e5f        nginx                  "nginx -g 'daemon of…"   10 minutes ago      Up 10 minutes                           k8s_nginx_nginx-deployment-6b474476c4-kdv22_default_d8b1d129-0525-4c45-8b65-96c3061196ec_0
e56192c9a715        k8s.gcr.io/pause:3.2   "/pause"                 10 minutes ago      Up 10 minutes                           k8s_POD_nginx-deployment-6b474476c4-kdv22_default_d8b1d129-0525-4c45-8b65-96c3061196ec_0
ubuntu@node01:~$ sudo docker inspect --format '{{ .State.Pid }}' 5b68dd0c5e5f
2744

### Run `ip a` in the container's network namespace to check its interface:
ubuntu@node01:~$ sudo nsenter -t 2744 -n ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
3: eth0@if12: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8951 qdisc noqueue state UP group default
```

#### Networking between (pods on different) nodes

The source IP of the pod changes to the actual node's IP
when it's communicating from node to node.

![cross-node-networking](images/k8s-outside-node-networking.png)

That node to node communication is done via the `cni` interface. 

A CNI goes on top of the existing network (as network overlay) 
and allows to essentially build tunnel between nodes.

A Container Network Interface (CNI) is an easy way to ease 
communication between containers in a cluster. The CNI has many 
responsibilities, including IP management, encapsulating packets,
and mappings in userspace. 

The networking plugin of choice (flannel, weave-net, etc.) that is
deployed while bootstrapping the cluster provides that CNI interface.

##### Command Reference

| Command | Description |
| :---    | ---:        |
| `docker inspect --format '{{ .State.Pid }}' <CONTAINER-ID>` | Gets the PID of the container                         | 
| `nsenter -t <CONTAINER-PID> -n <COMMAND>`                   | Executes command in the container's network namespace |


## Service Networking

![service-diagram](images/k8s-service-diagram.png)

Services allow our pods to move around, get deleted, and replicate, 
all without having to manually keep track of their IP addresses 
in the cluster. \
This is accomplished by creating one gateway (endpoint)
to distribute packets evenly across all pods.

Services create that endpoint and kube-proxy then updates the iptables
rules on the nodes accordingly to route to it.
 
Services create endpoints - objects on the API server that keep
cache of the IP addresses of the pods in that service.

Services come in different types:

* **ClusterIP** - Exposes the Service on a cluster-internal IP. 
Choosing this value makes the Service only reachable from within
the cluster. This is the default.

* **NodePort** - Exposes the Service on each Node’s IP at a static
 port (the NodePort). A `ClusterIP` Service, to which the `NodePort` 
 Service routes, is automatically created. You’ll be able to contact
 the `NodePort` Service, from outside the cluster, by requesting 
 `<NodeIP>:<NodePort>` 

* **LoadBalancer** - Exposes the Service externally using a cloud
provider’s load balancer. `NodePort` and `ClusterIP` Services, to
which the external load balancer routes, are automatically created.

* **ExternalName** - Maps the Service to the contents of the 
`externalName` field (e.g. foo.bar.example.com), by returning a 
CNAME record with its value. No proxying of any kind is set up.

**NOTE**: You can also use `Ingress` to expose your Service. 
`Ingress` is not a Service type, but it acts as the entry point 
for your cluster. It lets you consolidate your routing rules into 
a single resource as it can expose multiple services under 
the same IP address.

_**Example of a NodePort service:**_

```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-nodeport
spec:
  type: NodePort
  ports:
  - protocol: TCP
    port: 80
    targetPort: 80
    nodePort: 30080
  selector:
    app: nginx
```

##### Command Reference

| Command | Description |
| :---    | ---:        |
| `kubectl get svc -o yaml`         | View the services' specs            | 
| `sudo iptables-save \| grep KUBE` | View the K8S related iptables rules |


#### Load Balancers

The `LoadBalancer` type service is utilizing `NodePort` logic to serve
traffic. However the NodePort is chosen automatically.

_**Example of a LoadBalancer service:**_
```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-lb
spec:
  type: LoadBalancer
  ports:
  - port: 80
    targetPort: 80
  selector:
    app: nginx
```

_**Example creating such service without YAML:**_ \
`kubectl expose deployment nginx --port 80 --target-port 80 --type LoadBalancer`

Packets sent to Services with `Type=LoadBalancer` are source NAT’d by
default, because all schedulable Kubernetes nodes in the `Ready` state
are eligible for load-balanced traffic. So if packets arrive at a node 
without an endpoint, the system proxies it to a node with an endpoint, 
replacing the source IP on the packet with the IP of the node. \
This introduces latency (extra hop) and the _source IP*_ is not preserved.

To fix that you can annotate the service with:\
`externalTrafficPolicy=Local`

_**Example:**_ \
`kubectl annotate service nginx-lb externalTrafficPolicy=Local`

However this results in unbalanced traffic (i.e. if you have plenty of
pods on one node but only 1 pod another) => The service will 
distribute evenly between the two nodes. \

If you want properly balanced traffic use: \
 `externalTrafficPolicy=Cluster`.
 
**NOTE**: Whether the Source IP will be preserved actually depends
 on the specific cloud provider used for the Load Balancers.
 
#### Ingress Rules

Ingress consists of:
 * controller - the backend used (usually load balancer)
 * resource - the rules to follow when routing traffic
 
Ingress exists in the application layer. \
Therefore it can serve as central endpoint that can then route
traffic to different services based on the client's request.

![ingress](images/k8s-ingress.png)

_**Example (ingress resource with 3 rules):**_

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: service-ingress
spec:
  rules:
  - host: kubeserve.example.com
    http:
      paths:
      - backend:
          serviceName: kubeserve
          servicePort: 80
  - host: app.example.com
    http:
      paths:
      - backend:
          serviceName: nginx
          servicePort: 80
  - http:
      paths:
      - backend:
          serviceName: httpd
          servicePort: 80
```

Requests to `kubeserve.example.com` are redirected to 
the `kubeserve` service.

Requests to `app.example.com` are redirected to the `nginx` service.

Requests without hostname in the header (any traffic) are redirected
to the `httpd` service.

| Command | Description |
| :---    | ---:        |
| `kubectl get ingress`             | View the ingress resources            | 
| `kubectl describe ingress <NAME>` | View the ingress rules for a resource |
| `kubectl edit ingress <NAME>`     | Edit the ingress rules for a resource |


## Cluster DNS

In order for all hostnames inside the cluster (i.e. the ones in the 
ingress rules above) to be resolved we need to deploy DNS plugin.

As of v1.13 the `CoreDNS` plugin is the new default DNS solution for K8S.

![dns](images/k8s-dns.png)

#### Using sample busybox pod to troubleshoot DNS:

_**manifest:**_
```yaml
apiVersion: v1
kind: Pod
metadata:
  name: busybox
  namespace: default
spec:
  containers:
  - image: busybox:1.28.4
    command:
      - sleep
      - "3600"
    imagePullPolicy: IfNotPresent
    name: busybox
  restartPolicy: Always
```

_**command:**_

`kubectl exec -it busybox -- nslookup <SOMETHING>`


#### Headless services

These are service defined with `ClusterIP: None`. \
They are used when you don't need load balancing. \
Instead DNS replies with the IPs of the pods directly.

_**Example:**_
```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-headless
spec:
  clusterIP: None
  ports:
  - port: 80
    targetPort: 8080
  selector:
    app: nginx
```

#### Adjust the DNS policy

By default pods are deployed with `ClusterFirst` DNS policy 
(meaning they will inherit the DNS configuration from the node).

This can be modified if you want to add other name servers for a
pod like in the below manifest:

```yaml
apiVersion: v1
kind: Pod
metadata:
  namespace: default
  name: dns-example
spec:
  containers:
    - name: test
      image: nginx
  dnsPolicy: "None"
  dnsConfig:
    nameservers:
      - 8.8.8.8
    searches:
      - ns1.svc.cluster.local
      - my.dns.search.suffix
    options:
      - name: ndots
        value: "2"
      - name: edns0
```

When `dnsPolicy` is set to "None" you must provide a `dnsConfig`
(as above) and this is reflected in the pod's `/etc/resolv.conf`.