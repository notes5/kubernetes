## Deployments 

Automates the management of pods.\
Specifies the desired state for a set of pods which the cluster will strive to maintain. 

With deployments you can do: 

-   **Scaling** - You can specify the number of replicas you want and the deployment 
will create or remove pods to meet that number. 

-   **Rolling Updates** - You can change a deployment (i.e. new version of the docker image to be used)
 and it will gradually replace existing containers with the new version to avoid downtime. 

-   **Self-Healing** - If one of the pods is (accidentally) destroyed the deployment
 will immediately spin up new one to replace it thus maintaining the configured desired state. 

#### Useful commands: 

List deployments: \
`kubectl get deployments`

Get deployment configuration and events: \
`kubectl describe deployment <deployment-name>`

## Services 

Services are abstraction layer on top of a set of replicated pods (usually created by a deployment). \
You should access pods via that service instead directly so that as pods come and go
 you still get uninterrupted access to whatever replicas are up at the time.\
This makes services the way to actually access something in K8S 
(either from the cluster itself (internally) or from outside (externally)). 

#### Useful commands: 

List services: \
`kubectl get svc`

Get service configuration and events: \
`kubectl describe svc <service-name>`