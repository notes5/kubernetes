## What are microservices? 

Small independent services that work together to form a whole application. 

K8S value is best seen when deploying microservices on it.\
Microservices application architecture is the opposite of monolithic. 

#### Benefits: 

-   **Scalability** - you can scale up only part of an application (specific microservice) 
that is under load instead of the whole application. 

-   **Cleaner code** - Easier to make change in one area of the application without breaking another part. 

-   **Reliability** - Problems in one area less likely to affect other areas 

-   **Variety of tools** - Different parts of an app can be built using different set of tools, languages, etc.