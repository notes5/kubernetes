## Containers and Pods 

Pods are the smallest and most basic building block of the K8S model.\
A pod consists of _**one or more**_ containers, storage resources and 
a unique IP address in the K8S cluster network.\
In order to run containers K8S _**schedules**_ pods to run on a cluster node. 
When a pod is _**scheduled**_ K8S will run the containers that are part of that pod. 

#### Useful Commands:

Run a pod from manifest:\
`kubectl apply -f <pod-definition.yml>`

Get pod info and events:\
`kubectl describe pod <pod-name>`

Delete a pod:\
`kubectl delete pod <pod-name>`

Exec command from pod:\
`kubectl exec <pod-name> -- <command>`

_Example: Check nginx version from an nginx pod_\
`kubectl exec nginx-7c9f54d5f9-d242c -- nginx -V`

## Clustering and Nodes 

**Control Plane** - control servers that manage the cluster and host the cluster API 

**Nodes** - the servers that actually run the containers for the application workload 

#### Useful Commands: 

List nodes: \
`kubectl get nodes`

Get node info and events: \
`kubectl describe node <node-name>`

## Networking in K8S 

The K8S networking model involves creating a virtual network across the whole cluster
 to make sure that pods can communicate with each other even when running on different nodes. 
This is separate from the actual physical network that connects the cluster nodes themselves.\
This virtual network can be created via different plugins with different capabilities 
(i.e. flannel, calico, Wave Net, knitter, etc.) 

#### Useful Commands:

List pods with details to get their IP addresses: \
`kubectl get pods -o wide`

## Architecture and Components 

#### Control Plane Components: 

-   `kube-apiserver` - exposes the k8s API (the primary interface of the cluster). 
kubectl actually operates against the K8S API behind the scenes. 

-   `etcd` - HA key:value store to contain/backup all cluster data. 
Distributed synchronized data storage for the cluster state. 

-   `kube-scheduler` - selects a node to run new pods on (schedules pods on nodes). 

-   `kube-controller-manager` - All the backend stuff. Runs these controller processes (notable mentions): 

    -   **Node controller** - notice / act when nodes go down 

    -   **Replication controller** - maintains the correct number of pods for each replication object 

    -   **Endpoints controller** - populates the endpoints object (that is, joins Services & Pods). 

    -   **Service Account & Token controllers** - create default accounts and API access tokens for new namespaces 

-   `cloud-controller-manager` - runs controllers that interact with the underlying cloud providers 

#### Node Components: 

-   `kubelet` - agent that executes / makes sure containers are running in a pod. 
Middleman between K8S API and the container runtime. 

-   `kube-proxy` - network proxy that maintains network rules on nodes via firewall rules. 

-   **container runtime** - the software responsible for running containers 
(Docker, containerd, CRI-O, etc.) 

#### Useful Commands:

List the system pods: \
`kubectl get pods -n kube-system`

Check the status of the kubelet service:\
`sudo systemctl status kubelet`