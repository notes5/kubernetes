## Cluster Architecture 

![cluster-architecture](images/cluster-architecture.png)

## Install and bootstrap: 

#### Prerequisites: 

* Disable swap and make sure minimum 2 CPU are available on each machine. 

* Add the container runtime and kubernetes repos for the correct distribution. 

* Install the needed packages: `kubeadm`, `kubelet`, `kubectl`, container runtime (i.e. `docker-ce`) 

* If using docker runtime make sure to configure the cgroups manager to be systemd: 
https://kubernetes.io/docs/setup/production-environment/container-runtimes/#cgroup-drivers 

* Optionally mark these packages to be on-hold to lock the version you are running
 (i.e. with `apt-mark hold <package>` on ubuntu) 

* Optionally install completion for `kubeadm` and `kubectl` for easy life (`bash-completion` package is necessary): 
    ```shell
    echo "source <(kubeadm completion bash)" >> .bashrc
    echo "source <(kubectl completion bash)" >> .bashrc
    ```

#### Bootstrapping: 

Run the below commands as regular user with sudo privileges: 

1. Initialize the cluster, specifying pod network range (in this example: 10.244.0.0/16): \
    `sudo kubeadm init --pod-network-cidr=10.244.0.0/16`

2. Setup local kubeconfig (also printed from `kubeadm` init in step 1):
    ```shell
    mkdir -p $HOME/.kube 
    sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config 
    sudo chown $(id -u):$(id -g) $HOME/.kube/config 
    ```

3. Verify: \
    `kubectl version` 

4. Join the other nodes with the `kubeadm join` command (printed from `kubeadm` init in step 1): \
    `sudo kubeadm join <IP>:6443 --token <TOKEN> --discovery-token-ca-cert-hash <HASH>` 

5. Verify join is successful (Status should be `NotReady`): \
    `kubectl get nodes`

6. Configure cluster-network (i.e. `flannel`):
    * Setup bridged packets to traverse iptables rules (on each node):\
    `echo "net.bridge.bridge-nf-call-iptables=1" | sudo tee -a /etc/sysctl.conf && sudo sysctl -p` 
    * Install flannel from its latest manifest (on the master node):\
    `kubectl apply -f https://github.com/coreos/flannel/raw/master/Documentation/kube-flannel.yml` 

7. Verify cluster status again (Status should be `Ready`): \
    `kubectl get nodes` 

 

 