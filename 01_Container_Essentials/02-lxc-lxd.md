The idea behind LXC (LinuX Containers) was to have as close as possible to
 full VM experience but without the overhead. 

So with LXC you can run only run system or application containers that can share the kernel of the host. 
So no Windows on LXC. Kind of like Solaris Zones. 

LXD is the new version of LXC (the daemon behind it) - management via 
single command line tool or through RestAPI, etc. 

`lxd init` -> setup for first run (network settings, etc.) 

`lxc image list` -> list lxc images 

`lxc remote list` -> list lxc image repos 

`lxc list` -> list containers 

`lxc launch <image> [<container-name>]` -> run container from image (and optionally set name) 

`lxc exec <container-name> -- <shell>` -> enter container with shell session