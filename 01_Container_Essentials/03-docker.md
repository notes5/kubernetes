## Components 

-   Docker CLI (client) 

-   RestAPI 

-   Docker daemon 

When you run command with CLI (i.e. docker run) it gets translated and posted to the API 
that docker daemon uses so it then can execute that command:

CLI -> API -> Daemon -> Action 

## Functionalities 

-   Portable across machines - Allows bundling of an application + its dependancies into single object (container) 

-   Application centric - Optimized for deployment of applications instead of machines 

-   Automatic build - Enables automatic assembly of containers from source code 

-   Versioning - git like capability to track versions and history of a container 

-   Component re-use - Any container can be re-used to create new image 

-   Sharing images - Registry (i.e. DockerHub) can be used to share public and private images 

-   Tool ecosystem - Defines an API for automating and customizing containers so integration with 
other tools can be possible. 

## Docker Images 

Follow the Copy-On-Write principal where if a modifications of a base image are necessary
 they are implemented on a layer on top of the base layer (so they are copied first). 

This results in docker images usually comprising of multiple layers and if there is an 
update to a docker image only the modified layers are re-downloaded locally. 

Docker images are configured via Dockerfile and each line in it is usually results in an image layer. 


`docker image pull <image:tag>` - pulls docker image from registry 

`docker history <image:tag>` - shows history for an image 

`docker images` - lists pulled images