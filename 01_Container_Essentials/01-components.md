## Chroot 

Isolated environment from the rest of the server. 

Folder whose subfolder follow the FHS (i.e. own bin, lib, etc. folders) 

User logged in a chrooted environment have access only to what is setup there (i.e. commands, librabries, etc.).
He cannot move out of its "jail" and access the system that is outside of the chroot folder. 

## Linux Namespaces 

Allows the partitioning of kernel resources so that one set of processes sees only 
the resources allocated to it while another sees different set of resources. 

#### Types: 

-   **User**\
    Introduced in kernel 3.8. Security feature so that process in a user namespace can have PID 1 
    while still being mapped under process with PID 2000 on the host. 
    If by secuirty breach a process break the container it won't have root access to the host machine 
    regardless of its permissions in the container. 

-   **IPC (Inter-Process Communications)**\
    Isolates system resources from a process while giving processes in such namespaces visibility to each other
    (allowing inter-process communications). Basically allows for sets of  processes to exchange data
    while still being isolated. 

-   **UTS (Unix Time Share)**\
    Allows a single system to have different host and domain names to different processes. 
    Basically allows containers to have different hostname so that an app can identify them based on that. 

-   **Mount**\
    Controls which mountpoints are visible to each container. 

-   **PID**\
    Gives the ability of the processes in a container to have their own set of IDs... 
    so when you move a container on a different host not to worry about PID conflicts. 

-   **Network**\
    Virtualizes the network stack so that each container can have its own routing table, 
    filrewall rules, network devices, etc. 

-   **Cgroups**\
    Considered by some to be the seventh namespace. However the difference between 
    cgroup and namespace is that linux namespace limits the ability of a process to 
    **SEE** a system resource while cgroup limits the ability of a process to **ACCESS** a system resource. 

##Network Namespace 

`ip netns` \
Command to manage network namespaces 

Create a network namespace: \
`ip netns add <namespace-name>` 

List network namespaces: \
`ip netns list` 

Execute command in a network namespace \
`ip netns exec <namespace-name> <command>`

Execute a bash shell within a network namespace to run other commands against that namespace easier: \
`ip netns exec <namespace-name> bash` 

## Control groups 

Basically limits what one or more processes can access as resources from the host. 

#### Main cgroup subsystems: 

-   **blkio**\
    Allows you to limit (via throttle limits) and measures the I/O for a group of processes.  

-   **cpu**\
    Allows you to set weights and monitor CPU usage for a group of processes. 

-   **cpuacct**\
    Generates automatic reports on CPU resources used by a task in a cgroup. 

-   **cpuset**\
    Allows you to pin groups of processes to one CPU = Dedicates CPU to a cgroup task. 

-   **devices**\
    Allows you to allow/deny tasks in a cgroup to access a device(s). 

-   **freezer**\
    Allows you to suspend/resume tasks in a cgroup via SIGSTOP. 

-   **memory**\
    1MB memory = 256 memory pages (as 1 page = 4096 bytes).\
    Allows you to set memory limits (down to pages) to tasks in a cgroup and generates
     automatic reports on memory resources. 

-   **net_cls**\
    Tags network packages with classid so that packets from specific cgroup task can be identified. 

-   **net_prio**\
    Provides a way to set the priority of a network traffic dynamically.